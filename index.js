"use strict";

/*    1.   Опишіть своїми словами що таке Document Object Model (DOM)
        
            lдерево, по якому ми можемо подорожувати і взаємодіяти
            (window => DOM BOM JS)

      2.    Яка різниця між властивостями HTML-елементів innerHTML та innerText?
            
            innerHTML - вміст з тегами, якщо є
            innerText - тільки текст

      3.    Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
            
            за допомогою класів, айди, тегів... в залежності від задачі
                 */

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(
  (paragraph) => (paragraph.style.backgroundColor = "#ff0000")
);

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const parentElement = optionsList.parentElement;
console.log(parentElement);

const childNodes = optionsList.childNodes;
childNodes.forEach((node) => {
  console.log(node.nodeName, node.nodeType);
});

const testParagraph = document.getElementById("testParagraph");
testParagraph.innerHTML = "This is a paragraph";

const mainHeaderChilds = Array.from(
  document.getElementsByClassName("main-header")[0].children
);
mainHeaderChilds.forEach((el) => {
  el.classList.add("nav-item");
});
console.log(mainHeaderChilds);

const sectionTitleElems = Array.from(
  document.querySelectorAll(".section-title")
);
console.log(sectionTitleElems);

sectionTitleElems.forEach((el) => {
  el.classList.remove("section-title");
});
